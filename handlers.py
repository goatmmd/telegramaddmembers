from os import system
from time import sleep

from telethon.errors import PeerFloodError, \
    UserPrivacyRestrictedError, \
    UserIdInvalidError, \
    UserChannelsTooMuchError, UserBotError, UserAlreadyParticipantError, ChatIdInvalidError

from telethon.sync import TelegramClient

from telethon.tl.functions.channels import InviteToChannelRequest
from telethon.tl.functions.messages import AddChatUserRequest
from telethon import utils

from telethon.tl.types import InputUser

from config import read_setting, read_channel

from databasehandler import DataBaseHandler

client = TelegramClient('anon',
                        read_setting()['api_id'],
                        read_setting()['api_hash'],
                        connection_retries=100000000000,
                        retry_delay=6)

storage = DataBaseHandler()


def get_target_channel():
    for dialog in client.iter_dialogs():
        if dialog.is_group:

            if dialog.name == read_channel()['target_channel']:
                return dialog.id


def get_send_channel():
    for dialog in client.iter_dialogs():
        if dialog.name == read_channel()['send_channel']:
            real_id = utils.resolve_id(dialog.id)

            send_channel = real_id[0]

            return send_channel


def fetching_members(quantity, type):
    """
    Fetching members from our target channel that we have limit on it becasue
    sometime target group has over than 10000000 members and send request to API
    its bad for your account
    After that we are inserting their user.id and user.access_hash in database
    storing for later.

    :param quantity:
    :return:
    """
    usernames_list = list()
    with client:
        users_in_chat = client.get_participants(get_target_channel(), limit=300)

        for user in users_in_chat:
            usernames_list.append(user)

            storage.insert(tuple([user.id, user.access_hash]))

    if type == '1':
        adding_members_for_channel(quantity)
    else:
        adding_members_for_channel(quantity)


def adding_members_for_channel(quantity):
    """
    There's we are adding members that was fetching from our target channel
    we read their from our database, and we deploy them in for loop, and we have two method
    for add "AddChatUserRequest", "InviteToChannelRequest" method() and we use try except to catch any
    error from instagram

    :except PeerFloodError : That means You should sleep for 1 day
    :except UserPrivacyRestrictedError: The user's privacy settings do not allow you to add them
    :except UserIdInvalidError: The User info is Invalid
    :except UserChannelsTooMuchError: UserChannelsTooMuchError
    :param quantity:

    """
    system('cls')

    usernames = storage.read_fetchmany(quantity)

    counter = 0

    error_counter = 0

    with client:
        for user in usernames:
            if counter == 20:
                print('Enough For Today....See You Tomorrow ')

                sleep(86400)

                counter = 0

            if error_counter == 20:
                print('''You Cant Add Members Right Now\nMaybe You are got banned please check spam bot @SpamBot''')

            usr = InputUser(user[0], user[1])

            try:
                client(InviteToChannelRequest(channel=get_send_channel(), users=[usr]))

                counter += 1

                print(f'{counter} user added\tWaiting for 60 seconds')

                storage.remove(user[0], user[1])

                sleep(60)
            except PeerFloodError:
                print('''Getting Flood Error from telegram. Script is stopping now. Please try again after some time.
                        that means you have alot of send requests to TELEGRAM API.You Should  wait at least for
                        for day'''.ljust(100))

                print('Sleeping For One Day')

                sleep(86400)

                continue
            except UserPrivacyRestrictedError:
                print("ERROR: The user's privacy settings do not allow you to do this. Skipping.")

                storage.remove(user[0], user[1])

                continue
            except UserIdInvalidError:
                print('ERROR: The User info is Invalid')

                storage.remove(user[0], user[1])

                continue
            except UserChannelsTooMuchError:
                print('ERROR: UserChannelsTooMuchError')

                continue
            except UserBotError:
                print('ERROR: Bots can only added by admins in Channels')

                storage.remove(user[0], user[1])

                continue
            except UserAlreadyParticipantError:
                print('User already in Chat')

                storage.remove(user[0], user[1])

                continue
            except:
                print('ERROR: Something Went Wrong!')

                error_counter += 1

                storage.remove(user[0], user[1])

                continue

    print('all most Done'.upper())


def adding_members_for_group(quantity):
    """
    There's we are adding members that was fetching from our target channel
    we read their from our database, and we deploy them in for loop, and we have two method
    for add "AddChatUserRequest", "InviteToChannelRequest" method() and we use try except to catch any
    error from instagram

    :except PeerFloodError : That means You should sleep for 1 day
    :except UserPrivacyRestrictedError: The user's privacy settings do not allow you to add them
    :except UserIdInvalidError: The User info is Invalid
    :except UserChannelsTooMuchError: UserChannelsTooMuchError
    :param quantity:

    """
    system('cls')

    usernames = storage.read_fetchmany(quantity)

    counter = 0

    error_counter = 0

    with client:
        for user in usernames:
            if counter == 20:
                print('Enough For Today....See You Tomorrow ')

                sleep(86400)

                counter = 0

            if error_counter == 20:
                print('''You Cant Add Members Right Now\nMaybe You are got banned please check spam bot @SpamBot''')

            usr = InputUser(user[0], user[1])

            try:
                u = client.get_entity(usr)

                client(AddChatUserRequest(chat_id=get_send_channel(), user_id=u, fwd_limit=10))

                counter += 1

                print(f'{counter} user added\tWaiting for 60 seconds')

                storage.remove(user[0], user[1])

                sleep(60)
            except PeerFloodError:
                print('''Getting Flood Error from telegram. Script is stopping now. Please try again after some time.
                    that means you have alot of send requests to TELEGRAM API.You Should  wait at least for
                    for day'''.ljust(100))

                print('Sleeping For One Day')

                sleep(86400)

                continue
            except UserPrivacyRestrictedError:
                print("ERROR: The user's privacy settings do not allow you to do this. Skipping.")

                storage.remove(user[0], user[1])

                continue
            except UserIdInvalidError:
                print('ERROR: The User info is Invalid')

                storage.remove(user[0], user[1])

                continue
            except UserChannelsTooMuchError:
                print('ERROR: UserChannelsTooMuchError')

                continue
            except UserBotError:
                print('ERROR: Bots can only added by admins in Channels')

                storage.remove(user[0], user[1])

                continue
            except UserAlreadyParticipantError:
                print('User already in Chat')

                storage.remove(user[0], user[1])

                continue
            except ChatIdInvalidError:
                print('Please stop the script and run with Channel Method!')

                break
            except:
                print('ERROR: Something Went Wrong!')

                error_counter += 1

                storage.remove(user[0], user[1])

                continue

    print('all most Done'.upper())

    client.disconnect()
