from handlers import *
from os import system

from databasehandler import DataBaseHandler
from config import reset_setting, read_channel, change_channels


class Handler:
    def __init__(self):
        self.db = DataBaseHandler()
        self.info = read_channel()

    def run(self):
        client.connect()

        all_users = self.db.read()

        print(f'\nWe have {len(all_users)} members,ready to add'.ljust(10))

        print(
            f'''Setting : Your Target channel is {self.info["target_channel"]} and your Send Channel is {self.info["send_channel"]}''')

        print("-" * 40)

        print('Add from old data enter "1" ')

        print('Fetching New members enter "2"')

        print('To reset  your setting enter "3"')

        print('Change channel enter "4"\n')

        user_choice = input('Enter your choice: ')

        quantity = int(input('How many user do you want to add: '))

        add_type = input('ADD TO GROUP "1"\nADD TO CHANNEL "2" : ')

        if user_choice == '1':
            if add_type == '1':
                adding_members_for_group(quantity)

                client.start()

                client.run_until_disconnected()
            elif add_type == '2':
                adding_members_for_channel(quantity)

                client.start()

                client.run_until_disconnected()
        elif user_choice == '2':
            fetching_members(quantity, add_type)

            client.start()

            client.run_until_disconnected()
        elif user_choice == '3':
            reset_setting()

            system('cls')

            Handler().run()
        elif user_choice == '4':
            change_channels()

            system('cls')

            Handler().run()
        else:
            self.run()
