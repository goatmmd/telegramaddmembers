import sqlite3


class DataBaseHandler:
    instance = None

    @classmethod
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super().__new__(*args, **kwargs)

        return cls.instance

    def __init__(self):
        self.conn = sqlite3.connect('chats_users.db')

        self.create()

    def create(self):
        self.conn.execute('''CREATE TABLE IF NOT EXISTS usernames(
                                user_id int,
                                user_hash int)''')

        self.conn.commit()

    def insert(self, values):
        self.conn.execute(f'INSERT INTO usernames VALUES(?,?)', values)

        self.conn.commit()

    def read_fetchmany(self, quantity):
        result = self.conn.execute('SELECT * FROM usernames').fetchmany(quantity)

        return result

    def read(self):
        result = self.conn.execute('SELECT * FROM usernames').fetchall()

        return result

    def remove(self, user_id, user_hash):
        self.conn.execute(f'DELETE FROM usernames WHERE user_id =  {user_id} AND user_hash = {user_hash}')

        self.conn.commit()
