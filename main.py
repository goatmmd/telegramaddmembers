from interface import Handler

if __name__ == "__main__":
    handler = Handler()

    print('Bot Started!'.upper())

    handler.run()
