import json
from os import system


def target_channel():
    monitoring_channel = input('please enter one group to get all members(WITHOUT any "@"): ')

    if monitoring_channel is None:
        return target_channel()
    return monitoring_channel


def get_send_channel():
    send_channel = input('please enter one channel to adding members(WITHOUT any "@"): ')

    if send_channel is None:
        return get_send_channel()
    return send_channel


def get_api_id():
    api_id = int(input('Please enter your API_ID: '))

    if api_id is None:
        return get_api_id()
    return api_id


def get_api_hash():
    api_hash = input('Please enter your API_HASH: ')

    if api_hash is None:
        return get_api_id()
    return api_hash


def read_setting():
    try:
        with open('local_setting/setting.json', 'r') as f:
            data = json.loads(f.read())

        return data
    except:
        return None


def read_channel():
    try:
        with open('local_setting/channel_setting.json', 'r') as f:
            data = json.loads(f.read())

        return data
    except:
        return None


def change_channels():
    channels = {
        'target_channel': target_channel(),
        'send_channel': get_send_channel()
    }
    if FileNotFoundError:
        system('mkdir local_setting')
    with open('local_setting/channel_setting.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(channels, ensure_ascii=True, indent=4))


def reset_setting():
    configuration = {
        'api_id': get_api_id(),
        'api_hash': get_api_hash()
    }
    if FileNotFoundError:
        system('mkdir local_setting')
    with open('local_setting/setting.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(configuration, ensure_ascii=True, indent=4))


if not read_setting():
    reset_setting()

if not read_channel():
    change_channels()
